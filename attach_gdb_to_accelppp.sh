#!/bin/sh

# put this in tmux
# thanks pespin

outfile="/root/accelppp_gdb.log"

get_ts() {
  echo $(date +'%Y-%m-%d-%H-%M-%S-%N')
}

# load environment (real notify function and its parameters, implement here
# your notification system
notify() {
  true
}

touch ./my_env
. ./my_env || true


# generic debug in accelppp -> src https://accel-ppp.readthedocs.io/en/latest/debugging/index.html
# about attach -> src http://sourceware.org/gdb/onlinedocs/gdb/Attach.html

if [ $(id -u) -ne 0 ]
  then echo "Please run as root"
  exit
fi

log_header="================== $(get_ts) ==================="
echo "$log_header" >> "$outfile"

# appropriate order to send stderr to stdout, and stdout to append file
#   -> src https://stackoverflow.com/questions/876239/how-to-redirect-and-append-both-stdout-and-stderr-to-a-file-with-bash/876242#876242
# 2020-7-7 this is multithreading program, from 'bt full' to 'thread apply all bt full'
gdb -ex 'set breakpoint pending on' -ex 'set confirm off' \
  -ex 'set pagination off' -ex 'b _exit' -ex 'c' -ex 'thread apply all bt full' \
  -ex "generate-core-file /media/dlogs/coredump-script-$(get_ts)" -ex 'q' \
  -p "$(pidof accel-pppd)" >> "$outfile" 2>&1

echo "$(get_ts) accelppp crashed/stopped now" >> "$outfile"
notify >/dev/null 2>&1
