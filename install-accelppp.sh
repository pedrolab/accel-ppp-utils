#!/bin/sh

## TODO : use cdist

set -e

if [ -n "$LAPTOP" ]; then
  src_dir="/tmp/accel-ppp"
else
  src_dir="/opt/accel-ppp"
fi

# ./type/__accel_ppp_deb_deps/manifest
# sysvinit-utils because of pidof to attach to pid with gdb
if [ -n "$INSTALL_DEPS" ]; then
  sudo apt install git build-essential cmake libsnmp-dev linux-headers-amd64 libpcre3-dev libssl-dev liblua5.1-0-dev sysvinit-utils
fi

# ./type/__accel_ppp_deb_build/manifest

sudo rm -rf "$src_dir"
#git_repo="https://git.code.sf.net/p/accel-ppp/code"
git_repo="https://gitlab.com/guifi-exo/accel-ppp"

git clone "$git_repo" "$src_dir" || true
cd "$src_dir"
git pull
git checkout exo
sudo apt remove -y accel-ppp || true
rm -rf "$src_dir/build"
mkdir build
cd build

cmake -DKDIR=/usr/src/linux-headers-$(uname -r) -DCPACK_TYPE=Debian10 -DBUILD_IPOE_DRIVER=TRUE \
     -DBUILD_VLAN_MON_DRIVER=TRUE -DRADIUS=TRUE -DNETSNMP=FALSE -DLUA=TRUE \
     -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX=/usr \
     -DCMAKE_C_FLAGS='-g -O0' -DMEMDEBUG=TRUE ..

make

sudo cp drivers/ipoe/driver/ipoe.ko /lib/modules/$(uname -r)
sudo cp drivers/vlan_mon/driver/vlan_mon.ko /lib/modules/$(uname -r)
sudo depmod -a
sudo modprobe vlan_mon
sudo modprobe ipoe
modpath='/etc/modules'
if grep -qs 'vlan_mon' "$modpath"; then
  sudo echo 'vlan_mon' | sudo tee -a "$modpath"
if grep -qs 'ipoe' "$modpath"; then
   sudo echo 'ipoe' | sudo tee -a "$modpath"

cpack -G DEB

# rsyslog config
accelppp_log='/media/dlogs/accel-ppp/accelppp-exo-systemd-syslog.log'
sudo touch "${accelppp_log}"
sudo tee /etc/rsyslog.d/accelpp-exo.conf <<EOF
if \$programname == 'accelppp-exo' then ${accelppp_log}
& stop
EOF
sudo systemctl restart rsyslog

sudo apt install "${src_dir}/build/accel-ppp.deb"
